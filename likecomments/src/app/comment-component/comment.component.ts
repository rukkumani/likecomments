import { Component,OnInit } from '@angular/core';
import { AdminService } from '../services/admin.services';
import { ToasterService} from 'angular5-toaster/angular5-toaster';
import { Router } from "@angular/router";

@Component({
  selector: 'comment-component',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class commentComponent implements OnInit{
userData:any[];
userWork:string;
userComments:any[];
username:any[];
commentByUser:string;
  constructor(private toaster:ToasterService,private adminService:AdminService,private router:Router){
  this.userData=this.adminService.getUserData();
  console.log(this.userData);
  this.adminService.getAlltheRegisteredUserData().subscribe(data=> {
  if(data.length>0){
this.username=data.map(function( obj ) {
                                      return obj.name;
                                  });;
  }
  })
}


  ngOnInit() {
  this.toaster.pop('success','Welcome!!!');
 this.uploadedData();
       }


 uploadedData(){
  this.adminService.allUploadedUserPost().subscribe(data=> {
                                                              if(!data||data.length<=0){
                                                                   this.toaster.pop('warning', 'No comments')
                                                               };
                                                               if(data.length>0){
                                                               this.userComments=data;
                                                               console.log("usercomments");
                                                               console.log(this.userComments);
                                                               }
                                          })
 }



uploadWorkData(post){
var newObj={
mail:null as string,
name:null as string,
post:null as string
};
if(this.userData&&this.userData.length>0&&post){
console.log(this.userData);
newObj.mail=this.userData[0].mailId
newObj.name=this.userData[0].name;
newObj.post=post
this.adminService.uploadUserPost(newObj).subscribe(data=> {
                    if(!data){
                         this.toaster.pop('warning', 'Not Successful')
                     };
                     if(data){
                     this.userWork='';
                     this.toaster.pop('success', 'Posted Successfully');
                     this.uploadedData();
                     }
})
}else{
 this.toaster.pop('warning', 'Please Enter the text')
}
}


likeCountIncrease(_id,count){
console.log(count);
console.log(_id)
var likeObj={
_id:null as string,
count:null as number
}
likeObj._id=_id;
likeObj.count=count;
this.adminService.likeCounts(likeObj).subscribe(data=> {
this.uploadedData();
})
}

sendingComment(usercomment,_id){
if(usercomment&&this.userData){
var comObj={
comments:null as string,
By:null as string,
}
comObj.comments=usercomment;
comObj.By=this.userData[0].name;
this.adminService.userCommentsOnPost(comObj,_id).subscribe(data=> {
this.commentByUser='';
this.uploadedData();
})

}
}

}