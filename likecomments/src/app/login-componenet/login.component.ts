import { Component } from '@angular/core';
import { Router } from "@angular/router";
import { AdminService } from '../services/admin.services';
import { ToasterService} from 'angular5-toaster/angular5-toaster';

@Component({
  selector: 'login-component',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class loginComponent {

    loginData={
    userEmailName:null as string,
    password:null as string
    }

    constructor(private router:Router,private toaster:ToasterService,private adminService:AdminService){
    }

    loginOath(){
    this.adminService.getOathLogin().subscribe(data=> {
    console.log(data);
    })
    }
    eventLogin(loginData){
    console.log(loginData);
    this.adminService.getAdminAccess(loginData).subscribe(data=> {
    console.log(data);
                     if(!data||data.length<=0){
                            this.toaster.pop('warning', 'Invalid Login')
                     };
                     if(data.length>0){
                        console.log(data)
                        this.adminService.setuserData(data)
                        this.router.navigate(['../comment']);
                     }
    })


    }

}
