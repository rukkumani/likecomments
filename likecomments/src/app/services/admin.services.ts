import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';



@Injectable()

 export class AdminService{
 userData=[];
constructor(private http:HttpClient){}

getAdminAccess(userdata):Observable<any>{
 return this.http.get('/verifyUser/'+userdata.userEmailName+'/'+userdata.password)
};

getOathLogin():Observable<any>{
console.log( "i am calling");
 return this.http.get('/auth/google');
}


setuserData(userData){
this.userData=userData;
}
getUserData(){
return this.userData;
}
uploadUserPost(postObj):Observable<any>{
 return this.http.post('/userPost',postObj)
}

allUploadedUserPost():Observable<any>{
 return this.http.get('/getAllPost')
}

likeCounts(likeObj):Observable<any>{
    return this.http.post('/increaseLikeCounts',likeObj)
}
userCommentsOnPost(usercomment,_id):Observable<any> {
    return this.http.post('/postUserComments/'+_id,usercomment);
}
postUserData(userdata):Observable<any> {
   return this.http.post('/registerUserDetails',userdata);
  }

 getAlltheRegisteredUserData():Observable<any> {
 return this.http.get('/registerUsername');
      }

}