import { Component } from '@angular/core';
import { AdminService } from '../services/admin.services';
import { ToasterService} from 'angular5-toaster/angular5-toaster';
import { Router } from "@angular/router";

@Component({
  selector: 'register-user',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

  loginData={
    name:null as string,
    mailId:null as string,
    password:null as string
    }

    constructor(private router:Router,private toaster:ToasterService,private adminService:AdminService){
    }
registerUser(userData){
this.adminService.postUserData(userData).subscribe(data=> {
    console.log(data);
                     if(!data){
                            this.toaster.pop('warning', 'Invalid Login');
                            this.router.navigate(['../login']);
                     };
                     if(data){
             this.toaster.pop('success', 'Saved Successfully')
             this.loginData={
                name:'',
                 mailId:'',
                 password:''
             }
             this.router.navigate(['../login']);
              }
    })
}

}
