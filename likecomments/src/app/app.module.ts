import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { BrowserModule } from '@angular/platform-browser';


/*component*/
import { AppComponent } from './app.component';
import { commentComponent } from './comment-component/comment.component';
import { loginComponent } from './login-componenet/login.component';
import { RegisterComponent } from './register-component/register.component'

import { HttpClientModule } from '@angular/common/http';

import { mainRouting } from './app.routing';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { AdminService } from './services/admin.services'
import {ToasterModule, ToasterService} from 'angular5-toaster/angular5-toaster';
import { FormsModule } from '@angular/forms';
import { MentionModule } from 'angular-mentions/mention';

@NgModule({
  declarations: [
    AppComponent,
    commentComponent,
    loginComponent,
    RegisterComponent
  ],
  imports: [
  BrowserAnimationsModule,
        BrowserModule,
        HttpClientModule,
        mainRouting,
        FormsModule,
        ToasterModule,
        MentionModule ,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot()

  ],
  providers: [AdminService,ToasterService],
  bootstrap: [AppComponent]
})
export class AppModule { }
