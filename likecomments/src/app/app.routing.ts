import { RouterModule,Routes } from '@angular/router';
import { loginComponent } from './login-componenet/login.component';
import { commentComponent } from './comment-component/comment.component';
import { RegisterComponent } from './register-component/register.component'


const mainRoutes: Routes=[

    {
    path:'',
     redirectTo: 'login',
      pathMatch: 'full'
    },
    {
    path:'login',
   component:loginComponent
    },
    {
    path:'comment',
    component:commentComponent
    },
    {
     path:'register',
     component:RegisterComponent
    },

    {
        path: '**',
        redirectTo: 'login',
     }

]
export const mainRouting=RouterModule.forRoot(mainRoutes, { useHash:true,onSameUrlNavigation:'reload'});