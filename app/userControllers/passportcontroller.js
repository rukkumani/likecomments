var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose');
module.exports = function (app){
            app.use('/', router);
        };
 var  passport = require('passport');

router.get('/auth/google', passport.authenticate('google', { scope : ['profile', 'email'] }));

    // the callback after google has authenticated the user
    router.get('/auth/google/callback',
            passport.authenticate('google', {
                    successRedirect : '/#/comment',
            }),  (req, res) => {
                       console.log("it is success");

                   });