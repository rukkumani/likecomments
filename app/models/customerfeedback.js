var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var customerFeedBackSchema = new mongoose.Schema(
  {
     customerName:String,
     customerContact:String,
     customerAbout:Number,
     customerService:Number,
    customerRecommend:Number,
    customerProblem:Number,
    customerBenefit:Number,
    customerRating:Number
},{collection:"feedBack"});
  mongoose.model('feedBack',customerFeedBackSchema);